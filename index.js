( async function (){

    function ajax({ url, method })  {
        // return new Promise( (resolve, reject) => {
        //     let xhr = new XMLHttpRequest();
        //     xhr.onreadystatechange = function()  {
        //         if (xhr.readyState === XMLHttpRequest.DONE) {
        //             if (xhr.status === 200) {
        //                 resolve( xhr.responseText );
        //             } else {
        //                 reject( xhr )
        //             }
        //         }
        //     };
        //     xhr.open( method, url, true);
        //     xhr.send();
        // });
    }

    const getVersion = async function ( url,  ) {
        let version = null;
        const data = await ajax( { url, method: "GET" } );
        if( data ){
            try{  version = JSON.parse( data ); } catch (e) {
                version = { variable: "v", code: data }
            }
        }
        if( version ) return version ;
    }

    const versionSource = document.currentScript.getAttribute( 'versionSource' );
    const jQuery = document.currentScript.getAttribute( 'versionSource' );
    let version = document.currentScript.getAttribute( 'version' );
    let main = document.currentScript.getAttribute( 'main' );

    if( versionSource !== undefined && versionSource !== null && versionSource.length > 0 )
        version = getVersion( versionSource );

    let appendLocation = document.head || document.getElementsByTagName('head')[0];


    if( version ){
        fnodeJS.withVersion( version );
        const text = `?fnode=true&${version.variable}=${version.code}`;

    }
    if( main !== undefined && main !== null && main.length > 0 ){
        module.imports( `${main}` , {resolve: false });
    }

})();