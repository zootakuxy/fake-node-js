(()=>{

    const Htaccess = function( ){
        this.create = function ( ) {
            //language=ApacheConfig
            return `
                Options +SymLinksIfOwnerMatch
                RewriteEngine On
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteCond %{REQUEST_FILENAME} !-d
                RewriteRule (.*) router.php?path=$1 [L]
            `;
        }
    }

    module.exports = new Htaccess();
})();
