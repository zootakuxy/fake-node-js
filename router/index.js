(()=>{
    const htaccess = require( './.htaccess.js' );
    const router = require ( './router.php' );
    const fs = require ( 'fs' );
    const Arguments = require( '../src/arguments' );

    const  args = new Arguments( true );

    args.argument = { name: "src", alias: "s", type: String, value: "" };
    args.argument = { name: "router", alias: "S", type: String, value: "fnode.router" };
    let src = args.get( "src" );
    let routerServer = args.get( "router" );

    if( routerServer && routerServer.length > 0 ){
        fs.mkdirSync( routerServer, { recursive: true } );
    } else {
        routerServer = "."
    }

    fs.writeFileSync( `${routerServer}/.htaccess`, htaccess.create() );
    fs.writeFileSync( `${routerServer}/router.php`, router.create( src ) );
})();