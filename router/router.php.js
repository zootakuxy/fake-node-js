(()=>{
    const Router = function(){
        this.create = function ( src ) {
            if( src && src.length >0 && src.substr( src.length-1, 1 ) !== "/" ) src+="/";
            else src = "";

            //language=php
            return `<?php
                    $path = $_GET["path"];
                    $path = explode( "/", $path );
                    $file = "";
                    
                    for( $i = 1; $i< count( $path); $i++ ){
                        $file = $file . $path[ $i ];
                        if( $i+1 < count( $path ) ) $file = $file."/";
                    }
//                    ob_start();
                    include ( __DIR__."/../${src}".$file );
//                    die( ob_get_clean() );`
                ;
        }
    }
    module.exports = new Router();
})();
