( async ()=>{
    const path = require( 'path' );
    const fs = require( 'fs' );
    const FileUtil = require( 'zootakuxy-lib-util/file-util' );

    const Arguments = require( './src/arguments' );
    const  args = new Arguments( true );

    const defaultProject = process.cwd();
    console.log( defaultProject );
    args.argument = { name: "project", alias: "p", type: String, value: defaultProject };
    args.argument = { name: "recursive", alias: "r", type: Boolean, value: false };
    args.argument = { name: "clean", alias: "c", type: Boolean, value: false };
    args.argument = { name: "extensions", alias: "e", type: String, multiple:true, value: [ ".html", ".php", ".xhtml" ] }


    const project = path.resolve( args.get( "project" ) );
    const recursive  = args.get( "recursive" );
    const clean  = args.get( "clean" );


    let extensions =  args.get( "extensions" );
    extensions.forEach((value, index, array) => {
        if( value.substr(0, 1) !== "." ) value = `.${value}`;
        value = `.fnode${ value }`;
        array[ index ] = value;
    });


    const test = new RegExp(`^.*(${extensions.join("|")}).*$`);

    console.log( "project-dir: ",  project );
    console.log( "recursive: ",  recursive );
    console.log( "extension: ",  extensions );
    console.log( "test: ",  test );

    FileUtil.loadAllFiles( project,  test, p1 => {
        console.log( p1 );
        if( clean ) fs.unlinkSync( p1.path );
    });

})();
