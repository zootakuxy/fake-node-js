const  ResourceVersionUpdate = require( './src/ResourceVersionUpdate' );
const Arguments = require( './src/arguments' );
const  args = new Arguments( true );

args.argument = { name: "project", alias: "p", type: String, required: true };
args.argument = { name: "recursive", alias: "r", type: Boolean, value: false };
args.argument = { name: "save", alias: "s", type: Boolean, value: false };
args.argument = { name: "save-keep", alias: "k", type: Boolean, value: false };
args.argument = { name: "extensions", alias: "e", type: String, multiple:true, value: [ ".html", ".php", ".xhtml" ] };

const project = args.get( "project" );
const recursive  = args.get( "recursive" );
const save  = args.get( "save" );
const keep  = args.get( "save-keep" );

const packageJson = require('./package.json');
const version = packageJson.version;

let extensions =  args.get( "extensions" );
extensions.forEach((value, index, array) => {
    if( value.substr(0, 1) !== "." ) value = `.${value}`;
    array[ index ] = value;
});

const test = new RegExp(`^.*(${extensions.join("|")}).*$`);

console.log( "version: ",  version );
console.log( "project-dir: ",  project );
console.log( "extensions: ",  extensions );
console.log( "recursive: ",  recursive );
console.log( "save: ",  save );
console.log( "save-keep: ",  keep );
console.log( "test: ",  test );


let rvu = new ResourceVersionUpdate({ recursive, project, test, version } );
rvu.selectors = {
    script: {
        query: "script",
        attrRef: "src",
        withName: ()=> packageJson.main,
        acceptor: function ( { reference} ) {
            return /.*fnode\/fake-node-js*/.test( reference );
        }
    }
};

rvu.update()






