const cheerio = require( 'cheerio' );
const fs = require( 'fs' );
const FileUtil = require('zootakuxy-lib-util/file-util');

class ResourceVersionUpdate {

    #_selectors = {};
    #_recursive;
    #_project;
    #_version;
    #_test;
    #_save;
    #_keep;
    #_external;

    constructor ( { recursive, project, test, version, save, keep, external } = {} ) {
        this.#_recursive = recursive;
        this.#_project = project;
        this.#_version = version;
        this.#_test = test;
        this.#_save = save;
        this.#_keep = keep;
        this.#_external = external;
    }

    set selectors ( selectors ){
        if( !selectors ) return false;
        if( typeof selectors !== "object" ) return false;
        const name = selectors[ "name" ];
        this.#_selectors[ name ] = selectors;
    }
    get selectors( ) { return this.#_selectors; }

    get version() { return this.#_version; }
    get external() { return this.#_external; }

    get query (){
        let query = "";
        Object.keys( this.selectors ).forEach( name =>{
            if( query.length > 0 ) query += ", ";
            query += this.selectors[ name ].query;
        });
        return query.trim();
    }

    update(){
        const logs = [];
        const log = function( ...args ){
            logs.push( args );
        }

        const show = function(){
            while ( logs.length > 0 ){
                console.log( ...logs.shift() );
            }
        }

        FileUtil.loadAllFiles( this.#_project, this.#_test, p1 => {

            logs.length = 0;
            log( "" );
            log( "file: ", p1.path );
            const filePath = p1.path.split( "/" );
            const extension = filePath[ filePath.length-1 ].split("." );
            const extensionName = extension.pop();
            if( extension.length > 1 ){
                let fNodeExtension = `*.fnode.${extensionName}`;
                let notTest = new RegExp( `^(?!.*[.]${fNodeExtension}$).*$` );
                if( !notTest.test( p1.path ) ) return;
            }

            let content = `${ fs.readFileSync(p1.path) }`;
            let $ = cheerio.load( content );

            const self = this;

            let hasFound = 0;
            let hasUpdate = 0;

            $( this.query ).each( function( i, e ){
                let update = null;
                let found = null;

                const element = $( this );
                const tagName = element[0].tagName;
                const attrRef = self.selectors[ tagName ].attrRef;
                const reference = $(this).attr(  attrRef );

                let isUrl  = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test( reference );
                let accept = (!isUrl || self.external) && self.selectors[ tagName ].acceptor( { element, tagName, reference, attrRef } );

                if( isUrl ){
                    console.log( { isUrl, accept, p1, reference })
                }

                if( accept ){
                    found = $( this ).attr( attrRef ).trim();
                    const path = found.split( "/" );
                    let currentName = path[ path.length -1].split( "?" )[0];

                    let name = currentName;
                    if( typeof self.selectors[ tagName ].withName === "function" )
                        name = self.selectors[ tagName ].withName({currentName, element, tagName, reference, attrRef, path }  );
                    if( !name ) name = currentName;

                    path[ path.length-1 ] = `${name}?v=${ self.version }`;
                    update = path.join( "/" ).trim();
                    if( found === update ) update = false;
                    if( update ) $( this ).attr( attrRef, update.trim() );

                    if( found ) hasFound ++;
                    if( update ) hasUpdate ++;

                    log( "found: ", (found)? found: "not found" );
                    if( !found ) return;
                    show();

                    log( "update: ", (update)? update: "[not need]" );
                    show();
                }
            });

            if( hasFound ) {
                console.log( "founds: "+ hasFound );
                console.log( "updates: "+ hasUpdate );
            }

            if( !hasUpdate ) return;

            if( this.#_save ){
                log( "save: ", `${p1.path}` );
                fs.writeFileSync( `${p1.path}`, $.root().html() );
            } else if( this.#_keep ) {
                extension.push( "fnode" );
                extension.push( extensionName );
                filePath[ filePath.length -1 ] = extension.join( "." );

                log( "save: ", filePath.join("/") );
                fs.writeFileSync( `${ filePath.join("/") }`, $.root().html() );
            }

            show();
        }, { recursive: this.#_recursive });
    }
}

module.exports = ResourceVersionUpdate;


