
const commandLineArgs = require( 'command-line-args' );

class Arguments {

    #_optionsDefinitions = [];
    #_partial;
    #_options;

    constructor( partial ) {
         this.#_partial = partial;
    }

    set argument( { name, alias, type, multiple, value, required } ){
        this.#_optionsDefinitions.push( { name, alias, type, multiple, value, required } );
    }

    /**
     * @param name
     * @return {*|String|number|boolean|undefined|String[]|Number[]|Boolean[]|undefined[]}
     */
    get( name ){

        if( !this.#_options ){
            this.#_options = commandLineArgs( this.#_optionsDefinitions, { partial: true } );
            this.#_optionsDefinitions.forEach( value => {
                if( value.required && this.#_options[ value.name ] === undefined ){
                   throw new Error( `--${ value.name } is required argument` );
                }
            });
        }

        let value = this.#_options[ name ];
        if( value === undefined ) value = this.getDefinition( name ).value;
        return value;
    }

    getDefinition( name ){
        if( !name ) return  this.#_optionsDefinitions;
        return this.#_optionsDefinitions.find(value => {
            return value.name === name;
        })
    }

    get optionsDefinitions() {
        return this.#_optionsDefinitions;
    }
}

module.exports = Arguments;