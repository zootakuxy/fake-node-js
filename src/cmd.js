const exec = require('child_process').exec;

module.exports = {
    run: function( command ){
        return new Promise((resolve, reject) => {
            exec(command, function(error, stdout, stderr){
                if( stdout ) resolve( stdout );
                else reject( error, stderr );
            });

        });

    }
}