const cmd = require( './cmd')
const GitVersion = class {

    major;
    minor;
    path;
    hash;
    hashShort;
    date;
    dir;
    revs;
    code;

    static async version( dir, start ){

        if( start === undefined || start === null  ) start = 1;
        start = start * 100;

        let cmdIsGit, cmdCount, cmdHashShort, cmdHash, cmdDate;

        if( dir ){
            cmdIsGit = `cd ${dir}; git rev-parse --is-inside-work-tree`;
            cmdCount = `cd ${dir}; git rev-list --all --count`;
            cmdHashShort = `cd ${dir}; git log --pretty=\"%h\" -n1 HEAD`;
            cmdHash = `cd ${dir}; git log --pretty=\"%H\" -n1 HEAD`;
            cmdDate = `cd ${dir}; git log -n1 --pretty=%ci HEAD`;
        } else{
            cmdIsGit = `git rev-parse --is-inside-work-tree`;
            cmdCount = `git rev-list --all --count`;
            cmdHashShort = 'git log --pretty="%h" -n1 HEAD';
            cmdHash = 'git log --pretty="%H" -n1 HEAD';
            cmdDate = 'git log -n1 --pretty=%ci HEAD';
        }

        try{
            const trim = function ( str ) { return str.trim(); }
            const exec = function ( command ) {  return cmd.run( command ) };

            const isGit = (await (cmd.run( cmdIsGit ))).trim() === "true";
            const hash = trim(await exec( cmdHash ));
            const hashShort = trim(await  exec( cmdHashShort ));
            let revs = Number( trim(await exec( cmdCount )) );
            let totalRev = revs;

            // const commitDate = new Date(trim(await exec( cmdDate )));
            // const commitDate.setTimezone(new DateTimeZone('UTC'));
            // const date = $commitDate->format('Y-m-d H:i:s' );

            totalRev = totalRev + start;

            const path = Math.trunc(totalRev%10);
            totalRev = (totalRev/10);
            const minor = Math.trunc(totalRev%10);
            totalRev = (totalRev/10);
            const major = Math.trunc(totalRev%10);

            const version = new GitVersion();

            version.major = major;
            version.minor = minor;
            version.path = path;
            version.hash = hash;
            version.hashShort = hashShort;
            version.revs = revs;

            // $this.date = date;
            version.dir = dir;

            version.code = `v${ version.major }.${ version.minor }.${ version.path }-${version.hashShort}`;

            return  version;

        } catch ( ex ) {
            console.log( ex );
        }

    }
};

module.exports = GitVersion;