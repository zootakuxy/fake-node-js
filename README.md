# DyloaderJS

Dynamic loader script for web page. Similar to [node-require].

## Usage

HTML:

``` html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="location/dyloader-js-[versionalize]"></script>
<script src="location/main-script.js"></script>
```

Declare (only once for each script)
``` js
// returnable script (Caller script will wait for exports of object)
// the returneds var of declare works relative the current script
const { require, exports, pathHelper,  resources, script } = dyLoaderJS.declare();

//void script
const { require, pathHelper,  resources, script } =  dyLoaderJS.declare( { returns: false } );
```

Require (the number of time it is necessary)
``` js
//simple require | extension of .js is optional
const object = require( 'relative-path-of-scritp/script[.js]' );

//list of scripts
const objects = require( [
    'relative-path-of-script-1[.js]',
    'relative-path-of-script-2[.js]',
    'relative-path-of-script-3[.js]',
    ...
] );

//multples named object in diferents scripts
const { object1, object2, object3 } = require ( {
    object1: "relative-path-of-object1[.js]",
    object2: "relative-path-of-object2[.js]",
    object3: "relative-path-of-object3[.js]",
    ...
} );

//Relative to location of page
const ... = dyLoaderJS.require( ... );
```

Exports (only once for each script)

``` js
// exports a class
class Person {
    ...
}
exports( Person );

// exports a object
class Car {
    ...
}
exports( new Car ( "Toyta", "RAV4" ) );
```

With versionalize:

``` js
<<<<<<< HEAD
//in main script ...
dyLoaderJS.withVersion({
    variable: 'v',
    code: '1.0.0'
});
//<script src="path-of-requirede-script?v=1.0.0"></script>
```
Require multiple scripts in parallel mode and save timeout (recommended only if there is no dependency between scripts)

``` js
// opts.parallel default is false
const lists = require( [...], { parallel: true } );
const objects = require( {...}, { parallel: true } );
```

Scripts exemple:

``` js
( async () =>{
    const { require, exports, pathHelper,  resources, script } = dyLoaderJS.declare();

    const Person = await require('../relative-path/person');

    class Student extends Person{
        ...
    };

    exports( Student );
})();
```

Void Scripts exemple:

``` js
( async()=>{
    const { require, pathHelper,  resources, script } = dyLoaderJS.declare( { returns: false } );

    const Person = await require('../relative-path/person');
    let person1 = new Preson ( "Jão Maria" ); 
    person1.kill();
})()
```
