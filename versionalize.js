( async ()=>{
    const path = require( 'path' );
    const fs = require( 'fs' );


    const  ResourceVersionUpdate = require( './src/ResourceVersionUpdate' );
    const Arguments = require( './src/arguments' );
    const  args = new Arguments( true );

    const defaultProject = process.cwd();
    args.argument = { name: "project", alias: "p", type: String, value: defaultProject };
    args.argument = { name: "recursive", alias: "r", type: Boolean, value: false };
    args.argument = { name: "save", alias: "s", type: Boolean, value: false };
    args.argument = { name: "save-keep", alias: "k", type: Boolean, value: false };
    args.argument = { name: "extensions", alias: "e", type: String, multiple:true, value: [ ".html", ".php", ".xhtml" ] };
    args.argument = { name: "version", alias: "v", type: String };
    args.argument = { name: "version-git", alias: "g", type: Boolean, value: false };
    args.argument = { name: "css", alias: "c", type: Boolean, value: false };
    args.argument = { name: "js", alias: "j", type: Boolean, value: false };
    args.argument = { name: "external", alias: "x", type: Boolean, value: false };
    args.argument = { name: "start", type: Number, value: 1 };

    const project = path.resolve( args.get( "project" ) );
    const recursive  = args.get( "recursive" );
    const save  = args.get( "save" );
    const keep  = args.get( "save-keep" );
    let version  = args.get( "version" );
    let gitVersion  = args.get( "version-git" );
    const css  = args.get( "css" );
    const js  = args.get( "js" );
    const start = args.get( "start" );
    const external = args.get( "external" );

    if( !version && !gitVersion ){
        gitVersion = true;
    }

    let extensions =  args.get( "extensions" );
    extensions.forEach((value, index, array) => {
        if( value.substr(0, 1) !== "." ) value = `.${value}`;
        array[ index ] = value;
    });
    const test = new RegExp(`^.*(${extensions.join("|")})$`);

    if( gitVersion && version ) throw new Error( "conflict: can't use version and version-git in simultaneous" );

    if( gitVersion ){
        version = ( await require('./src/git.version').version( project, start ) ).code
    }

    console.log( "version: ",  version );
    console.log( "start: ",  start );
    console.log( "project-dir: ",  project );
    console.log( "extensions: ",  extensions );
    console.log( "recursive: ",  recursive );
    console.log( "css: ",  css );
    console.log( "js: ",  js );
    console.log( "external: ",  external );
    console.log( "save: ",  save );
    console.log( "save-keep: ",  keep );
    console.log( "test: ",  test );

    let rvu = new ResourceVersionUpdate({ project, recursive, version: version, save, keep, test, external } );

    if( js ) rvu.selectors = {
        name: "script",
        query: "script",
        attrRef: "src",
        acceptor: function ( { reference} ) {
            return /.*.js*/.test( reference ) && !( /.*fnode\/fake-node-js*/.test( reference ) );
        }
    };

    if( css ) rvu.selectors = {
        name: "link",
        query: "link",
        attrRef: "href",
        acceptor: function ( { reference} ) {
            return /.*.css*/.test( reference );
        }
    };

    if( Object.keys( rvu.selectors ).length > 0 ){
        console.log( "update..." )
        rvu.update();
    }
    else throw new Error( "Especifica o tipo de extensão a atualizar" );

})();
