(()=>{
    const req = require( '../router/router.php' );
    const lines = req.create().split( "\n");
    let start;
    for ( let i = 0; i< lines.length; i++ ){
        if( lines[ i ].trim() === "" ) continue;
        if( !start ){
            let tl = lines[ i ].trimLeft();
            start = lines[ i ].length - tl.length;
            // clean = lines[ i ].substr( 0, start );
        }
        let line = lines[ i ];
        line = line.substr( start, line.length );
        console.log( line );

    }

})();
