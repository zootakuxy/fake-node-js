/**
 * @param scriptManager
 * @constructor
 * @author zootakuxy (Daniel Costa)
 * @since 2019-12-09
 * @license OpenSource
 */
( async function fnJS(){
    const VENDOR = 'vendor';
    const DIR = 'dir';
    const ROOT = 'root';
    const fakeJQuery = {
        ajax: function ( { url, method}) {
            return new Promise( (resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function()  {
                    if (xhr.readyState === XMLHttpRequest.DONE) {
                        if (xhr.status === 200) {
                            resolve( xhr.responseText );
                        } else {
                            reject( xhr )
                        }
                    }
                };
                xhr.open( method, url, true);
                xhr.send();
            });
        },
        type: function ( data ) {
            if( typeof data === "string" ) return "string";
            else if( typeof data === "function" ) return "function";
            else if( typeof data === "boolean" ) return "boolean";
            else if( typeof data === "bigint" ) return "bigint";
            else if( typeof data === "number" ) return "number";
            else if( typeof data === "object" && Array.isArray( data ) ) return "array";
            else if( data === undefined || data === null ) return "undefined";
            else if( data === 0 ) return "number";
            else if( typeof data === "symbol") return "symbol";
            else if( typeof data === "object") return "object";
            else if( typeof data === "object") return "object";
        },

        param: function ( data ) {
            if( !data ) return "";
            return  Object.keys( data ).map(function(k) {
                return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
            }).join('&');
        }
    };

    const getVersion = async function ( url ) {
        let version = await fakeJQuery.ajax({
            url: url,
            method: "GET"
        });

        if( version ){
            try{  version = JSON.parse( version ); } catch (e) {
                version = { variable: "v", code: version }
            }
        }
        return version;
    }

    /**
     * @param src { String }
     * @param vendor { String }
     * @return {RelativePathHelper}
     * @constructor
     */
    const RelativePathHelper =  function RelativePathHelper( { src, vendor } ){

        let script = FNodeScript.instance({
            src: src
        });

        this.useVendor = function ( newVendor ) {
            vendor = newVendor;
        };

        /**
         * @param src
         * @param extension
         * @param query
         * @param resolve
         * @return {string | null }
         */
        this.resolvePath = ({ src, extension, query, resolve = true }) => {
            let resolved;
            if( resolve ){
                if( !src ) return null;
                let srcParts = src.split('/');
                let root = "";
                let dir = script.dir;
                let useVendor = vendor? `${ vendor }` : script.dir;

                if( srcParts[ 0 ] === "" ) resolved = `${ root }${ src }`;
                else if( srcParts[ 0 ] === '.' ) resolved = `${ dir }/${ src.substr( 1, src.length )}`;
                else if( srcParts[ 0 ] === '..' ) resolved = `${ dir }/${ src }`;
                else resolved = `${ useVendor }/${ src }`;

                if ( typeof extension === "string" && extension.length >0 && resolved.toLowerCase().substr(resolved.length - extension.length, extension.length) !== extension)
                    resolved = `${resolved}${extension}`;

            } else resolved = src;
            src =  RelativePathHelper.normalize(resolved);
            if( query !== null && query !== undefined ){
                query = fakeJQuery.param( query );
                if( query.length > 0 ){
                    src += `?${query}`
                }
            }
            return src;
        };

        /**
         * @param src
         * @param query
         * @param resolve
         * @return {string}
         */
        this.js = (src, { query, resolve = true } = {}) => this.resolvePath({ src: src, extension: '.js', query, resolve });

        /**
         * @param src
         * @param query
         * @param resolve
         * @return {string}
         */
        this.css = (src, { query, resolve = true } = {} ) => this.resolvePath({src: src, extension: '.css', query, resolve });

        /**
         * @param src
         * @param query
         * @return {string}
         */
        this.php = (src, { query } = {} ) => this.resolvePath( { src: src, extension: '.php', query });

        /**
         * @param src
         * @return {string}
         */
        this.dir = (src ) => {
            let dir = this.resolvePath({ src: src, extension: null });
            if( dir === '' ) dir = "./";
            if( dir.substr( dir.length-1, 1 ) !== '/' ) dir+='/';
            return dir;
        };

        /**
         * @param src
         * @param query
         * @return {string}
         */
        this.others = (src, { query } = {}) => this.resolvePath({ src: src, extension: null, query });

        return this;
    };

    RelativePathHelper.locationOf = ( src )=>{
        let parts = src.split( '/' );
        if ( parts[ 0 ] === '' ) return ROOT;
        else if( parts[ 0 ] ===  '.' ) return DIR;
        else if( parts[ 0 ] ===  '..' ) return DIR;
        else return VENDOR;
    };

    RelativePathHelper.isIndex = ( src ) =>{
        let parts = src.split( '/' );
        if ( parts.length < 1 ) return false;
        parts = parts[ parts.length-1 ].split( '.' );
        if ( parts.length < 1 ) return false;
        return parts[ 0 ] === 'index';
    };

    RelativePathHelper.isOf = ( src, extension ) =>{
        let parts = src.split( '/' );
        if ( parts.length < 1 ) return false;
        parts = parts[ parts.length -1 ].split( '.' );
        if ( parts.length < 2 ) return false;
        return parts[ parts.length -1 ] === extension;
    };

    RelativePathHelper.isDir = ( src ) =>{
        if( !src || src.length <= 0 ) return  false;
        let parts = src.split( '/' );
        return parts.length > 0 && parts[ parts.length -1 ] === '';
    };

    /**
     * @param fullPath
     * @return {string}
     */
    RelativePathHelper.normalize = ( fullPath ) => {
        let pathParts = fullPath.split( '/' );
        let pathNormalizedParts = [];
        let realNames = 0;

        for( let index=0; index <pathParts.length; index++ ){
            const nextPart = pathParts[ index ];
            if( nextPart === '' && index ) continue;
            if( nextPart === '.' ) continue;
            if( nextPart === '..' && realNames > 0 && pathNormalizedParts.length >0 ){
                pathNormalizedParts.pop();
                realNames--;
            } else pathNormalizedParts.push( nextPart );
            if( nextPart !== '..' ) realNames ++;
        }

        let normalized="";

        for( let index=0; index <pathNormalizedParts.length; index++ ){
            const next = pathNormalizedParts[ index ];
            if( !normalized || normalized.length === 0 ) normalized = next;
            else normalized += next;
            if( index+1 < pathNormalizedParts.length ) normalized +='/';
        }
        return `${normalized}`;
    };

    RelativePathHelper.sourcesParts = ( src ) => {
        let srcParts = src.split( '?' );
        if( srcParts.length <= 0 ) return null;
        return {
            src: srcParts[ 0 ],
            query: srcParts[ 2 ]
        }
    };

    /**
     * @param exports { function ( Object ): boolean }
     * @param require
     * @param pathHelper { RelativePathHelper }
     * @param scriptManager { ScriptManager }
     * @param script { FNodeScript }
     * @return {RexAgents}
     * @constructor
     */
    const RexAgents  = function RexAgents ( { exports, require, pathHelper, scriptManager, script } ){

        /**
         * Resolvedor das dependencis JS
         * @type {function( ( String ) ): *}
         * */
        this.require = require;

        /**
         * Resolvedor dos recursos css, html, images...
         */
        this.resources = {
            css: async  ( imports, opts= { parallel: true, resolve: true } ) => {
                return scriptManager.createImportsPromise( "css", imports, opts, pathHelper, script );
            }
        };

        /**
         * Controllador de modulo
         */
        this.module = {
            /**
             * @param imports { String | Object | String[] }
             * @param opts {{parallel:boolean, resolve:boolean}}
             * @return {Promise<*>}
             */
            imports : async  ( imports, opts= { parallel: false, resolve: true } ) => {
                return await scriptManager.createImportsPromise("js", imports, opts, pathHelper, script );
            },

            /**
             * Obter o script que importou a dependencia
             * @return {null}
             */
            get importer() { return script.importer },

            _exports: null,
            _exported: false,
            _packageName: "",
            _class: null,

            /**
             * Obter o valor expotado pro modulo
             * @return {null}
             */
            get exports (){ return this._exports; },

            /**
             * Definir o valor que o modulo ira exportar na proxima requisição
             * @param values
             */
            set exports( values ){
                this._exports = values;
                this._exported = true;
                exports( this.exports );
            },

            /**
             * Definir o nome do pacode pro modulo
             * @param packageName
             */
            set package( packageName ){ this._packageName = packageName; },

            /**
             * Obter o nome do pacote do modolu
             * @return {string}
             */
            get package () { return this._packageName; },

            /**
             * Definir um nome da classe pra modulo
             * @param className{ String }
             */
            set className( className ) { this._class = className },

            /**
             * Obter o nome da classe definida pro modulo
             * @return {String}
             */
            get className( ) { return  this._class; },

            /**
             * Resolvedor de caminho para o script atual
             * @type {RelativePathHelper}
             */
            pathHelper: pathHelper
        };

        return this;
    };

    /**
     * @param src { String }
     * @param dir { String }
     * @param fileName { String }
     * @param js
     * @param query
     * @param version
     * @return {FNodeScript}
     * @constructor
     */
    const FNodeScript = function ({ src, dir, fileName, js, query, version } ) {
        this.src = src;
        this.query = query;
        this.version = version;
        this.dir = dir;
        this.js = js;
        this.fileName =  fileName;
        this.alias = [ ];
        this.importer = null;

        /**  @type { null|{type, attrRef, url, attrs }} */
        this.elementType = null;

        /** @type { RelativePathHelper } */
        this.pathHelper = null;
        this.isDeclared = false;
        this.isDeclared  = false;
        this.returns = false;

        let _object = null;
        let _isExported = false;

        this.object = () => _object;

        this.isExported = () => !!( _isExported );

        this.exports = object => {
            _object = object;
            _isExported = true;
        };

        this.hasAlias =( src ) =>{
            for( let i = 0; i < this.alias.length; i++ ) {
                if( this.alias[ i ] === src ) return this;
            }
            return  false;
        };

        return this;
    };

    /**
     * @param src
     * @param scriptManager { ScriptManager }
     * @return {FNodeScript}
     */
    FNodeScript.instance = ( { src, scriptManager } ) =>{
        let script;

        if( !src ) src = document.currentScript.getAttribute( 'src' );

        if( scriptManager && ( script = scriptManager.findScriptBySrc( src ) ) ) return script;

        let srcParts = RelativePathHelper.sourcesParts( src );

        let pathParts = srcParts.src.split( '/' );
        let dir = "";
        for( let i = 0; i < pathParts.length -1; i++ ){
            dir += `${ pathParts[ i ]}`;
            if( i+2 < pathParts.length ) dir+= "/";
        }

        let js = document.currentScript;

        let version;
        if( srcParts.query && srcParts.query.length >0 ) version = srcParts.query;

        return new FNodeScript({
            src: srcParts.src,
            dir: dir,
            fileName: pathParts[pathParts.length - 1],
            js: js,
            query: srcParts.query,
            version: version
        });
    };

    /**
     * @return {ScriptManager}
     * @constructor
     */
    const ScriptManager = function ScriptManager () {

        /**@type {FNodeScript[]} */
        let _cached = [];
        let _pendents = {};
        let _appendAt = null;
        let _version;
        let _vendor;

        this.setVersion = ( { variable, code, name } ) => {
            variable = variable || 'v';
            code = code || '1.0';
            _version = {
                variable: variable,
                code: code,
                name: name
            }
        };

        this.setVendor = ( vendorDir )=>{
            _vendor = vendorDir;
            _cached.forEach( script =>{
                if( script && script.pathHelper ) script.pathHelper.useVendor( _vendor );
            });
        };

        /**
         * @param srcQuery
         * @returns {FNodeScript}
         */
        this.findScriptBySrc =( srcQuery )=>{
            let { src, query } = RelativePathHelper.sourcesParts( srcQuery );
            for( let i = 0; i < _cached.length; i++ ){
                let script = _cached[ i ];
                if( script.src === src ) return script;
                if( script.hasAlias( src ) ) return script;
            }
            return null;
        };

        /**
         * @param script { FNodeScript }
         * @param asName
         */
        this.createAlias =( script, asName )=>{
            script.alias.push( asName );
        };

        /**
         * Registar um novo package
         * @param script {FNodeScript}
         * @returns { null|boolean }
         */
        this.registerPackage = ( script ) => {
            if( this.findScriptBySrc( script.src ) ) return false;
            _cached.push( script );
            return true;
        };

        /**
         * @return {FNodeScript}
         */
        this.findScriptByDocument = ( ) => {
            if( !document.currentScript ) throw new Error( 'Script not available' );
            let src = document.currentScript.getAttribute( 'src' );
            return  this.findScriptBySrc( src );
        };

        /**
         * Informar a todas as dependecia em espera que o pacote ja possui um objecto
         * @param script {FNodeScript}
         * @param object
         * @param exports
         * @return {boolean}
         */
        this.publish = ( script, object, exports ) => {
            if( script.isExported() ) return false;
            exports( object );
            this.publishNext( script.src, script );
            script.alias.forEach( src => this.publishNext( src, script ));
            return  true;
        };

        /**
         * @param src
         * @param script { FNodeScript }
         * @return {boolean}
         */
        this.publishNext = ( src, script )=>{

            let pendent = _pendents[ src ];
            if( !pendent ) return true;

            script.returns = true;
            pendent.forEach( onNextLoad =>{
                onNextLoad( script );
            });
            delete _pendents[ src ];
        };

        /**
         * @param elementType {{ type, attrRef }}
         * @param fullPath
         * @param appendLocation { HTMLElement }
         */
        this.getElementPosition = function ( elementType, fullPath, appendLocation ) {
            let paths = [ `${elementType.type}://${fullPath}::${elementType.attrRef}` ];

            _cached.forEach( ( next )=>{
                if( !next.elementType ) return;
                paths.push( `${next.elementType.type }://${next.src}::${next.elementType.attrRef}`  );
            });
            paths.sort();
            paths.reverse();
            let beforeSrc, beforeElement;

            for (let i = 0; i < paths.length; i++) {
                if( i-1 > 0 ) beforeSrc = paths[ i-1 ];
                if( paths[ i ] === `${elementType.type}://${fullPath}::${elementType.attrRef}` ) break;
            }

            if( beforeSrc ){
                let arr = beforeSrc.split('://');
                let before = { type: arr[0], name: arr[1] };
                arr = arr[1].split( "::" );
                before.attRef =  arr[ arr.length -1 ];
                before.name = before.name.substr(0, before.name.length - (before.attRef.length + 2 ) );
                let items = appendLocation.getElementsByTagName( before.type );
                for (let i = 0; i < items.length; i++) {
                    const  element = items.item(i);
                    if( element.getAttribute( before.attRef ).split("?")[ 0 ] === before.name ){
                        window.lasted = element;
                        beforeElement = element;
                        return beforeElement;
                    }
                }
            }
            return null;
        };

        /**
         * @param resource { string }
         * @param src { String }
         * @param onLoad { function ( script: FNodeScript )}
         * @param pathHelper { RelativePathHelper }
         * @param importerScript { FNodeScript }
         * @param ops { {resolve:boolean}}
         * @param solveSource
         * @param original
         */
        this.processNext =  ( { resource, src, pathHelper, importerScript, ops, onLoad, solveSource, original  } ) => {

            const location = RelativePathHelper.locationOf( src );

            if( typeof onLoad !== "function" ) throw new Error( "Parado ai babaca, ti peguei filha da puta" );

            let version = '';
            const query = {};

            if( original ) {
                if( RelativePathHelper.isIndex( src ) ) query.index = true;
                // query.retry =`${location}`;
            }

            if( _version ) version = `?${ _version.variable }=${ _version.code }`;
            if( version ) query[ _version.variable ] = _version.code;

            if( RelativePathHelper.isDir( src ) ) src = `${src}/index.${resource}`;

            const elementsTypes = {
                js: { type: "script", attrRef: "src" },
                css: { type: "link", attrRef: "href",
                    attrs:{ rel: "stylesheet" }
                },
            };

            let elementType = elementsTypes[ resource ];
            elementType.url = pathHelper[ resource ]( src, { resolve: ops.resolve, query } );
            const scriptName = elementType.url.split("?")[ 0 ];
            let requiredScript = this.findScriptBySrc( scriptName );
            let pendent = _pendents[ scriptName ];

            // Quando o script já foi exportado
            if( requiredScript && requiredScript.isExported() ) {
                if( solveSource ) this.createAlias( requiredScript, solveSource );
                onLoad( requiredScript );

            // Quando o script já foi requerido mais ainda não completou a requisição
            } else if( ( !requiredScript || !requiredScript.isExported() ) && pendent ){
                if( solveSource ) this.createAlias( requiredScript, solveSource );
                if( !solveSource ) pendent.push( onLoad );

            // Script que nunca foi requerido
            } else {
                pendent = [ onLoad ];
                if( !solveSource ) _pendents[ scriptName ] = pendent;

                let newElement = document.createElement( elementType.type );
                newElement[ elementType.attrRef ] = `${ elementType.url }` ;/*${ version }${ retry }*/

                if( elementType.attrs ) Object.keys( elementType.attrs ).forEach( ( next ) =>{
                    newElement[ next ] = elementType.attrs[ next ]
                });

                let appendLocation = _appendAt || document.head || document.getElementsByTagName('head')[0];

                let endAction = ( success, event ) => {
                    let script =  scriptManager.findScriptBySrc( scriptName );
                    if( !script ) {
                        script = FNodeScript.instance({  src: scriptName });
                        this.registerPackage( script );
                    }
                    script.elementType =  elementType;

                    if( solveSource ) this.createAlias( script, solveSource );

                    if( script.isExported() && solveSource  ) this.publishNext( solveSource, script );
                    else if( !success || !script.isDeclared || script.object() || ( script.isDeclared && !script.returns ) ){
                        this.publish( script, script.object(), script.exports );
                    }
                };

                let script =  scriptManager.findScriptBySrc( scriptName );
                if( !script ) {
                    script = FNodeScript.instance({ src: scriptName });
                    this.registerPackage( script );
                }

                if( importerScript ) script.importer = importerScript.src;

                newElement.onload = ( event) => endAction( true, event );
                newElement.onerror = ( event) => {
                    if( !solveSource ) solveSource = elementType.url;
                    if( !original ) original = src;

                    const retry = { resource, pathHelper, importerScript, ops, onLoad, solveSource, original };

                    if( location === VENDOR && !RelativePathHelper.isIndex( src ) && !RelativePathHelper.isOf( src, resource ) ) {
                        retry.src = `${original}/index.${resource}`;
                        this.processNext( retry );

                    } else if( location === VENDOR ) {
                        retry.src = `./${original}`;
                        this.processNext( retry );

                    } else if( location === DIR && !RelativePathHelper.isIndex( src ) && !RelativePathHelper.isOf( src, resource ) ){
                        retry.src = `./${original}/index.${resource}`;
                        this.processNext( retry );
                    }
                    else endAction( false, event );
                    newElement.remove();
                };
                const before = this.getElementPosition( elementType, scriptName, appendLocation );
                appendLocation.insertBefore( newElement, before );
            }
        };

        /**
         * @param src
         * @param pathHelper { RelativePathHelper }
         * @param retry { int }
         * @param retryLast { string }
         * @param retryNext { string }
         */
        this.requireOf = ( src, pathHelper , retry, retryLast, retryNext  ) =>{
            retry = retry? retry: 0;

            let fullPath = pathHelper.js( src );
            let importedScript = this.findScriptBySrc( fullPath  );
            let pendent = _pendents[ fullPath ];
            let location = RelativePathHelper.locationOf( src );

            if( importedScript && importedScript.isExported() ) {
                return importedScript.object();
            } else if( !importedScript && location === VENDOR ){
                return this.requireOf( `./${src}`, pathHelper, retry++, VENDOR, DIR );
            } else if( !importedScript ){
                throw new Error( `Script ${ fullPath } not imported` );
            } else if( ( !importedScript.isExported() ) && pendent ) {
                throw new Error( `Script ${ fullPath } not waited` );
            } else {
                throw new Error( `Require error` );
            }
        };

        /**
         * @param resource{ string }
         * @param require { String | Object | String[] }
         * @param opts { { parallel: boolean, resolve: boolean } }
         * @param pathHelper { RelativePathHelper }
         * @param importerScript { FNodeScript}
         * @return {Promise<*>}
         */
        this.createImportsPromise = ( resource, require, opts, pathHelper , importerScript ) =>  new Promise((resolve, reject) => {
            if ( !require ) { resolve(null); }
            let  isString =  fakeJQuery.type( require ) === 'string';
            let isArray = fakeJQuery.type( require ) === 'array';
            let isObject = fakeJQuery.type( require ) === 'object';

            if ( require && ( isObject || isArray || isString )) {
                let list, response, parallel = (opts && opts.parallel);

                if( isString ){
                    list = [ require ];
                    response = null;
                } else if( isObject && Object.keys(require).length >0 ){
                    list = Object.keys(require);
                    response = { };
                } else if( isArray && require.length > 0 ){
                    list = require;
                    response = [];
                } else resolve( null );

                let nextRequire = ( i ) => {
                    let next = list[ i++ ];
                    let src = ( isObject )? require[ next ] : next;
                    let hasNext = i < list.length;
                    this.processNext( { resource, src, pathHelper, importerScript, ops: opts,
                        onLoad: ( script ) => {
                            if( isString ) response = script.object();
                            else if( isObject ) response [ next ] = script.object();
                            else response.push( script.object() );
                            if( hasNext && !parallel ) nextRequire( i );
                            else if( !hasNext ) resolve( response );
                        }
                    });
                    if( hasNext && parallel ) nextRequire( i );
                };
                nextRequire( 0 );

            } else resolve( null );
        });

        /**
         * @param _scriptManager { ScriptManager }
         * @param src
         * @param register
         * @return { RexAgents }
         */
        this.agentsCreator = ( _scriptManager, { src, register } ) =>{
            /** @type FNodeScript*/
            let script;

            let pathHelper = new RelativePathHelper( {
                src: src,
                vendor: _vendor,
            } );

            if( src ) script = this.findScriptBySrc( src );
            else script = this.findScriptByDocument( );

            if( script === null ){
                script = FNodeScript.instance({
                    src: src,
                    scriptManager: this
                });
                if( register ) this.registerPackage( script );
            }

            script.pathHelper = pathHelper;

            let require = function( src ){
                return _scriptManager.requireOf( src, pathHelper );
            };

            return new RexAgents({
                require: require,
                script: script,
                scriptManager: _scriptManager,
                exports: object =>{
                    this.publish( script, object, script.exports )
                },
                pathHelper: pathHelper
            });
        };

        return this;
    };

    const scriptManager = new ScriptManager();

    /**
     * @return {FakeNodeJS}
     * @constructor
     */
    const FakeNodeJS = function ( ) {

        /**
         * Resolver as dependencia a partir de root
         */
        const root = scriptManager.agentsCreator( scriptManager, { src: "./", register: false } );

        /**
         * Resolver as dependencia a partir de root
         */
        root.root = root;


        this.withVersion = ( { variable, code, name } ) => {
            scriptManager.setVersion(  { variable: variable, code: code, name: name } );
        };

        /**
         * Obter a versao pra adicionar nas dependencias
         * @param url A localizacao do arquivo da versao
         */
        this.withVersionSource = async ( url ) => { this.withVersion( await getVersion( url ) ) };

        /**
         * Indicar a localizaçao dos vendor (livrarias externas ex node_modules)
         * @param vendorDir
         */
        this.withVendor  = function ( vendorDir ) {
            scriptManager.setVendor( vendorDir );
        };

        /**
         * Declarar o script e obter os agentes de controlo de dependencia (agentes: require, module, resources)
         * @param returns { Boolean } indicar se o script vai ter retorno (o script dependente vai esperar ate que o retorno seja exportado)
         * @param imports
         * @param opts
         * @return {RexAgents}
         */
        this.declare = ( { returns, imports, opts } = { returns: true }) => {
            if( !document.currentScript ) throw new Error( 'Script not available' );
            if( !document.currentScript.getAttribute('src' ) ) throw new Error( 'Source of script is not set' );

            if( returns === null || returns === undefined ) returns = true;
            let script = scriptManager.findScriptByDocument( );

            if( !script ){
                script = FNodeScript.instance({
                    returns: returns,
                    scriptManager: scriptManager
                });
                script.returns = returns;
                script.isDeclared = true;
                scriptManager.registerPackage( script );
            } else if( !script.isDeclared ) {
                script.returns = returns;
                script.isDeclared = true;
                script.js = document.currentScript;
            } else throw  new Error( `Script ${ script.src } already declared` );
            const agents = scriptManager.agentsCreator( scriptManager, { register: true } );

            agents.root = root;
            return agents;
        };
        return this;
    };

    const fnodeJS = new FakeNodeJS( );

    /** @deprecated use fnodeJS*/
    window.dyLoaderJS = fnodeJS;
    window.fnodeJS = fnodeJS;

    Object.defineProperty( window, "fnodeJS", {
        get() { return fnodeJS }, configurable: false
    });

    let { module } = fnodeJS.declare( { returns: true } );
    module.exports = ( fnodeJS );

    const versionSource = document.currentScript.getAttribute( 'versionSource' );
    let version = document.currentScript.getAttribute( 'version' );
    let main = document.currentScript.getAttribute( 'main' );
    let router = document.currentScript.getAttribute( 'router' );

    if( versionSource !== undefined && versionSource !== null && versionSource.length > 0 )
        version = await getVersion( versionSource );

    if( !version )  version = { variable: "v", code: "v1.0.0", name: "app"  }


    if( version ) fnodeJS.withVersion( version );
    console.log( version );
    if( main !== undefined && main !== null && main.length > 0 ){
        if( router && router.length > 0 ) main = `${router}/${version.code}/${main}`;
        module.imports( `${main}` , {resolve: false });
    }
})();