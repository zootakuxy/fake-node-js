<?php


function version(){

    include_once __DIR__.'/GitAppVersion.php';
    $gitDir = __DIR__."/../../";

    $start = 1;
    if( isset( $_GET[ "start"] ) ) $start = (int) $_GET[ "start" ];
    elseif( isset( $_POST[ "start"] ) ) $start = (int) $_POST[ "start" ];

    $gitVersion = new GitAppVersion( $gitDir, $start );
    die( json_encode( $gitVersion, JSON_UNESCAPED_UNICODE ) ."\n");
};

version();