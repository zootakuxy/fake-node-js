<?php

class GitAppVersion {

    public $major;
    public $minor;
    public $path;
    public $code;
    public $date;
    public $hash;
    public $hashShort;
    public $gitDir;

    /**
     * GitAppVersion constructor.
     * @param $gitDir
     * @param $start int
     */
    public function __construct( $gitDir, $start ) {
        if( !$start ) $start = 1;
        $start = $start * 100;


        if( $gitDir ){
            $cmdIsGit = "cd $gitDir; git rev-parse --is-inside-work-tree";
            $cmdCount = "cd $gitDir; git rev-list --all --count";
            $cmdHashShort = "cd $gitDir; git log --pretty=\"%h\" -n1 HEAD";
            $cmdHash = "cd $gitDir; git log --pretty=\"%H\" -n1 HEAD";
            $cmdDate = "cd $gitDir; git log -n1 --pretty=%ci HEAD";
        } else{
            $cmdIsGit = "git rev-parse --is-inside-work-tree";
            $cmdCount = "git rev-list --all --count";
            $cmdHashShort = 'git log --pretty="%h" -n1 HEAD';
            $cmdHash = 'git log --pretty="%H" -n1 HEAD';
            $cmdDate = 'git log -n1 --pretty=%ci HEAD';
        }

        try{
            $isGit = trim(exec( $cmdIsGit )) == "true";
            $hash = trim(exec( $cmdHash ));
            $hashShort = trim(exec( $cmdHashShort ));
            $totalRev = (int) trim(exec( $cmdCount ));

            $commitDate = new DateTime(trim(exec( $cmdDate )));
            $commitDate->setTimezone(new DateTimeZone('UTC'));
            $date = $commitDate->format('Y-m-d H:i:s' );

            $totalRev = $totalRev + $start;

            $path = $totalRev%10;
            $totalRev = $totalRev/10;
            $minor = $totalRev%10;
            $totalRev = $totalRev/10;
            $major = $totalRev%10;

            $this->major = $major;
            $this->minor = $minor;
            $this->path = $path;
            $this->hash = $hash;
            $this->hashShort = $hashShort;

            $this->date = $date;
            $this->gitDir = $gitDir;

            $this->code = sprintf('v%s.%s.%s-%s', $this->major, $this->minor, $this->path, $this->hashShort );;
        } catch (Exception $ex ) {
            die( $ex->getTraceAsString() );
        };


    }

}